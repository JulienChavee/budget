<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AccountForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'account.form.name'))
            ->add('number', TextType::class, array('label' => 'account.form.number'))
            ->add('submit', SubmitType::class, array('label' => 'account.form.save'))
            ->getForm();
    }
}
<?php

namespace App\Repository;

use App\Entity\AccountMovement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccountMovement|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountMovement|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountMovement[]    findAll()
 * @method AccountMovement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountMovementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountMovement::class);
    }

//    /**
//     * @return AccountMovement[] Returns an array of AccountMovement objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountMovement
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\ClassData;

use Symfony\Component\Validator\Constraints as Assert;

class AccountData
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     *
     * @var string
     */
    public $name;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max="20")
     * @var string
     */
    public $number;
}

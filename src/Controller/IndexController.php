<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $user=$this->getUser();
        $em=$this->getDoctrine()->getManager();

        if(!is_null($user)) {
            $accounts = $em->getRepository('App:Account')->findBy(array('user' => $user->getId()));

            $expenses = array();
            $revenues = array();

            $totalExpenses = 0;
            $totalRevenues = 0;

            foreach ($accounts as $k => $account) {
                foreach ($account->getMovements() as $key => $movement) {
                    if ($movement->getValue() > 0) {
                        $totalRevenues += $movement->getValue();
                        $revenues[] = $movement;
                    } elseif ($movement->getValue() < 0) {
                        $totalExpenses += $movement->getValue();
                        $expenses[] = $movement;
                    }
                }
            }

            return $this->render(
                'index/index.html.twig',
                array(
                    'expenses' => $expenses,
                    'revenues' => $revenues,
                    'totalExpenses' => $totalExpenses,
                    'totalRevenues' => $totalRevenues
                )
            );
        }
        else
            return $this->render('index/index.html.twig');
    }
}

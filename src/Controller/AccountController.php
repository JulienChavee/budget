<?php

namespace App\Controller;

use App\Entity\Account;
use App\ClassData\AccountData;
use App\Form\AccountForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if(!is_null($user))
        {
            $accounts = $em->getRepository('App:Account')->getAccountByUser($user->getId());

            return $this->render(
                'account/index.html.twig',
                array(
                    'accounts' => $accounts,
                )
            );
        }

        // TODO return if no user
    }

    /**
     * @Route("/account/ajax/accountForm", name="account_ajax_accountform")
     * @param Request $request
     * @return JsonResponse
     */
    public function accountForm(Request $request)
    {
        $user = $this->getUser(); // TODO : check if connected

        if(!is_null($request->get('accountID')))
            $data = ''; // TODO
        else
            $data = new AccountData();

        $form = $this->createForm(AccountForm::class, $data);
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            if($form->isValid()) {
                try {
                    $account = new Account();
                    $account->setName($data->name);
                    $account->setAccountNumber($data->number);
                    $account->setUser($user);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($account);
                    $em->flush();

                    return new JsonResponse(
                        array(
                            'success' => true
                        )
                    );
                } catch (\Exception $e) {
                    return new JsonResponse(
                        array(
                            'success' => false,
                            'errors' => 'Unkown error' // TODO : translate
                        )
                    );
                }
            } else {
                $errors = array();

                foreach( $form->getErrors(true) as $error)
                    $errors[$error->getOrigin()->getParent()->getName().'_'.$error->getOrigin()->getName()] = $error->getMessage();

                return new JsonResponse(
                    array(
                        'success' => false,
                        'errors' => $errors // TODO : translate
                    )
                );
            }
        }

        return new JsonResponse(
            array(
                'form' => $this->renderView(
                    'account/forms/account.html.twig',
                    array(
                        'form' => $form->createView(),
                        'formType' =>  'new'
                    )
                )
            )
        );
    }

    /**
     * @Route("/account/ajax/delete", name="account_ajax_delete")
     * @param Request $request account_ajax_delete
     * @return JsonResponse
     */
    public function deleteAccount(Request $request)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
// TODO : check if request id isset && if user is connected
        $account = $em->getRepository('App:Account')->findOneBy(array('id' => $request->get('id')));

        if($account) {
            if($account->getUser() === $user) {
                try {
                    $em->remove($account);
                    $em->flush();

                    // TODO catch exception

                    return new JsonResponse(
                        array(
                            'success' => true
                        )
                    );
                } catch(\Exception $e) {
                    return new JsonResponse(
                        array(
                            'success' => false,
                            'error' => 'Unknown error' // TODO Translate
                        )
                    );
                }
            } else {
                return new JsonResponse(
                    array(
                        'success' => false,
                        'error' => 'Account not found' // TODO Translate
                    )
                );
            }
        } else {
            return new JsonResponse(
                array(
                    'success' => false,
                    'error' => 'Account not found' // TODO Translate
                )
            );
        }
    }
}

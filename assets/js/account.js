let routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import Toast from '../../assets/js/Toast.js';
Routing.setRoutingData(routes);

$('#accountAddAccount').on('click', function() {
    $.ajax({
        type: 'POST',
        url: Routing.generate('account_ajax_accountform'),
        dataType: "json",
    })
    .done(function (data) {
        let modal = $('#defaultModal');
        modal.html(data.form);

        modal.modal('show');
    })
    .fail(function () {
        //console.log("test");// TODO
    });
});

$('#defaultModal').on('click', '#buttonFormSubmit',function() {
    let modal = $('#defaultModal');
    let form = $('form[name="account_form"]');
    let submitButton = $(this);

    submitButton.prop('disabled', true);
    submitButton.data('oldText', submitButton.text());
    submitButton.html($('<i>').addClass('fas fa-spinner fa-spin'));

    $.ajax({
        'type' : 'POST',
        'url' : Routing.generate('account_ajax_accountform'),
        'dataType' : "json",
        'data' : form.serialize()
    })
    .done(function(data) {
        if(data.success === true) {
            // TODO :  Move toast maker in dedicated js class

            /*let toast = $('<div>');
            toast.addClass('toast')
                .prop('role', 'alert')
                .prop('aria-live', 'assertive')
                .prop('aria-atomic', 'true');

            let toastHeader = $('<div>');
            toastHeader.addClass('toast-header')
                .append(
                    $('<strong>').text('Successfull update').addClass('mr-auto')
                )
                .append(
                    $('<small>').addClass('text-muted').text('just now')
                )
                .append(
                    $('<button>')
                        .prop('type', 'button')
                        .prop('aria-label', 'Close')
                        .addClass('ml-2 mb-1 close')
                        .attr('data-dismiss', 'toast')
                        .append(
                            $('<span>')
                                .prop('aria-hidden', 'true')
                                .append(
                                    $('<i>').addClass('far fa-times fa-xs')
                                )
                        )
                );

            let toastBody = $('<div>');
            toastBody.addClass('toast-body')
                .text('Your account was successfully created');

            toast.append(toastHeader)
                .append(toastBody);

            $('#toastsContainer').append(toast);

            $(toast).toast({'delay' : 3000});
            $(toast).toast('show');*/

            let toast = new Toast();
            toast.displayToast('Successfull update', 'Your account was successfully created');

            modal.modal('hide');
        } else {
            let modalAlertDanger = $('#modalAlertDanger');

            modalAlertDanger.addClass('d-none');

            form.removeClass('was-validated');
            form.addClass('was-validated');
            form.find('.invalid-feedback').remove();

            let elements = form.find('*').filter(':input');
            let elementIDsInError = Object.keys(data.errors);

            elements.each(function(index, element) {
                if(!elementIDsInError.includes(element.id))
                    element.setCustomValidity('');
            });

            if(typeof data.errors === 'string') {
                modalAlertDanger.text(data.errors);
                modalAlertDanger.removeClass('d-none');
            } else {
                $.each(data.errors, function (name, message) {
                    let element = form.find('#' + name);
                    // TODO : if error isn't related to element but is global
                    element[0].setCustomValidity(message);
                    element.after(
                        $('<div>').addClass('invalid-feedback').text(message)
                    )
                });
            }
        }
    })
    .always(function() {
        submitButton.text(submitButton.data('oldText'));
        submitButton.prop('disabled', false);
    });

    // TODO : catch .error
});

$('body').on('click', 'button[data-action]', function() {
    switch($(this).data('action')) {
        case 'delete':
            let modalConfirmation = $('#confirmationModal');

            modalConfirmation.find('.modal-confirmation-yes').data('type', 'account');
            modalConfirmation.find('.modal-confirmation-yes').data('id', $(this).data('id'));

            modalConfirmation.modal('show');

            break;
    }
});

$('.modal-confirmation-yes').on('click', function() {
    let id = $(this).data('id');

    $.ajax({
        'type' : 'POST',
        'url' : Routing.generate('account_ajax_delete'),
        'dataType' : "json",
        'data' : {
            'id' : id
        }
    })
    .done(function(data) {
        if(data.success === true) {
            // TODO :  Move toast maker in dedicated js class

            let toast = $('<div>');
            toast.addClass('toast')
                .prop('role', 'alert')
                .prop('aria-live', 'assertive')
                .prop('aria-atomic', 'true');

            let toastHeader = $('<div>');
            toastHeader.addClass('toast-header')
                .append(
                    $('<strong>').text('Successfull delete').addClass('mr-auto')
                )
                .append(
                    $('<small>').addClass('text-muted').text('just now')
                )
                .append(
                    $('<button>')
                        .prop('type', 'button')
                        .prop('aria-label', 'Close')
                        .addClass('ml-2 mb-1 close')
                        .attr('data-dismiss', 'toast')
                        .append(
                            $('<span>')
                                .prop('aria-hidden', 'true')
                                .append(
                                    $('<i>').addClass('far fa-times fa-xs')
                                )
                        )
                );

            let toastBody = $('<div>');
            toastBody.addClass('toast-body')
                .text('Your account was successfully deleted');

            toast.append(toastHeader)
                .append(toastBody);

            $('#toastsContainer').append(toast);

            $(toast).toast({'delay' : 3000});
            $(toast).toast('show');

            $('div[data-id="' + id + '"]').fadeOut(500, "easeOutExpo", function() {
                $(this).remove();
            });
        } else {
            // TODO
        }
    });

    // TODO : catch .error
});
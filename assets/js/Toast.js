export default class Toast {
    constructor(options) {
        this.options= {
            'delay': 3000,
            'autohide': true,
            'animation': true
        };

        $.extend(this.options,options);
    }

    displayToast(headerText, bodyText, toastContainer='#toastsContainer') {
        let toast = $('<div>');
        toast.addClass('toast')
            .prop('role', 'alert')
            .prop('aria-live', 'assertive')
            .prop('aria-atomic', 'true');

        let toastHeader = $('<div>');
        toastHeader.addClass('toast-header')
            .append(
                $('<strong>').html(headerText).addClass('mr-auto')
            )
            .append(
                $('<small>').addClass('text-muted').text('just now')
            )
            .append(
                $('<button>')
                    .prop('type', 'button')
                    .prop('aria-label', 'Close')
                    .addClass('ml-2 mb-1 close')
                    .attr('data-dismiss', 'toast')
                    .append(
                        $('<span>')
                            .prop('aria-hidden', 'true')
                            .append(
                                $('<i>').addClass('far fa-times fa-xs')
                            )
                    )
            );

        let toastBody = $('<div>');
        toastBody.addClass('toast-body')
            .html(bodyText);

        toast.append(toastHeader)
            .append(toastBody);

        $(toastContainer).append(toast);

        $(toast).toast(this.options);
        $(toast).toast('show');
    }
}
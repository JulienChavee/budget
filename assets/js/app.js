/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');$

function enableThings() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    const isMobile = window.matchMedia("only screen and (max-width: 768px)");
}

/* Hiding all tooltip when clicking somewhere in page */
$('body').on('click', '*', function() {
    $('[data-toggle="tooltip"]').tooltip('hide');
});

$(function() {
    enableThings();
});

$(document).ajaxComplete(function() {
    enableThings();
});

